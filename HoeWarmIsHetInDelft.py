import requests as re
import re as reg
import bs4
import validators as vl
import getopt
import sys

#simple scrapper class to extract defined data units from a web
class WebDataScrapper:
   Headers = {}
   Link = None
   CssSelector = None
   
   #Value Mapping is used to map certain units, in a followig format "TextIndicator : Flag"
   # Text indicator indicates specific value Unit, with a flag. So all units related to Celcius should have TemperatureC flag for example
   ValueMapping = {
      "°C" : "TemperatureC",
      "°" : "TemperatureF"
   }
   
   IdentifiedValues = []
   
   def __init__(self,link, headers = None, ValueMapping = None):
      if vl.url(link) and isinstance(headers, dict | type(None)) and isinstance(ValueMapping, dict | type(None)):
         self.Link = link
         self.Headers = headers
         if isinstance(ValueMapping, dict):
            self.ValueMapping = ValueMapping
      else: 
         raise TypeError("Incorrect URL, or incorrect selector(string), header(dictionary) data type")

# Function scrape the pointed value and identify data specified by unit. 
# valueindicator needs to match one of the indicators mapped in the ValueMapping
# More precise the selector the more precise the result, otherwise it might return several temperature values. 
   def Scrape(self, valueindicator, cssSelector = None):
      
      response = re.get(self.Link, headers)
      
      #if no CssSelector is being used it will attempt to parse whole webpage content for values specified.
      if isinstance(cssSelector, str):
         parser = bs4.BeautifulSoup(response.text, "html.parser")
         values = parser.select(cssSelector)
         for ind, vl in enumerate(values):
            values[ind] = vl.text
      else:
         values = response.text.splitlines()
      
      # Loops through mapping and checks if a key is associated with a temperature in Celcius indicated by TemperatureC flag. 
      # This way it is possible to define diffrent data types or units
      if valueindicator in self.ValueMapping.values():
         for key in self.ValueMapping.keys():
            
            #Parsing of values related to the Celcius of Farenheit
            if self.ValueMapping[key] == valueindicator and (valueindicator == "TemperatureC" or valueindicator == "TemperatureF"):
               for vl in values:
                  regex = "(\d+\.*\d*)\s*" + key
                  temperatures = reg.findall(regex, vl)
                  if temperatures:
                     for tm in temperatures:
                        self.IdentifiedValues.append(str(tm + " " + key))
      else:
         print("No Valid Value indicator specified, specified value indicator is: ", valueindicator, ", and allowed value types are: ", self.ValueMapping.values())
         return
      
      return self.IdentifiedValues
      
   def ChangeURI(self, link): 
      if vl.url(link):
         self.Link = link
      else:
         raise ValueError("Incorect URI, provided URI is: ", link)
      
   def ChangeMapping(self, mapping):
      if isinstance(mapping, dict):
         self.ValueMapping = mapping
      else:
         raise TypeError("Type needs to be a dictionary")
   
   def PrintLastResult(self):
      return self.IdentifiedValues
   



# Execution of above scrapper class to search for the temperature. 
if __name__ == "__main__":
   headers = {
   "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36 OPR/98.0.0.0"
   }
   
   link = "https://weerindelft.nl/WU/55ajax-dashboard-testpage.php"
   selector = None
   indicator = "TemperatureC"
   
   ValueMapping = {
      "°C" : "TemperatureC",
      "°" : "TemperatureF"
   }

   opts, args = getopt.getopt(sys.argv[1:], "l:s:i:")
   
   for opt, arg in opts:
      if opt == "-l":
            link = arg
      if opt == "-s":
            selector = arg
      if opt == "-i":
            indicator = arg

   
   #Example for farrenheit
   #link = "https://weather.com/weather/tenday/l/Washington+DC?canonicalCityId=4c0ca6d01716c299f53606df83d99d5eb96b2ee0efbe3cd15d35ddd29dee93b2"
   #selector = "#detailIndex1 > summary > div > div > div.DetailsSummary--temperature--1kVVp > span.DetailsSummary--highTempValue--3PjlX"
   
   #link - to a scraped webpage,
   #ValueMapping - mapping of units and text indicators to identify the values
   Scraper = WebDataScrapper(link, headers)
   
   
   #indicator - used to indicate data flag mapped in ValueMapping,
   #selector - used to input CSS selectors
   temperature = Scraper.Scrape(indicator, selector)
   if temperature:
      for tmp in temperature:
         vl = tmp.split(' ')
         if Scraper.ValueMapping[vl[1]] == indicator:
            print(vl[0] + " degrees Celsius")