FROM python:3.10

ADD HoeWarmIsHetInDelft.py .

RUN pip3 install bs4
RUN pip3 install requests
RUN pip3 install validators

ENTRYPOINT ["python", "./HoeWarmIsHetInDelft.py"]